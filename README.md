#Themes for SDDM display manager

Theme background images are named to give them some sort of connection to their reference, ie: animegirl... etc. The theme is then given the same name as the image.

Cookie cutter refers to the fact that all themes have pretty much come from the same TEMPLATE. Generally, the only or major difference between them will be the background image. Some may have different text color, size, or font but that is only a small number. They use two colors for the shutdown and reboot buttons in the top right corner of the screen. Red or Yellow are used where needed to provide sufficent contrast making them more visible on the background.

You must make sure plasma-framework is installed to insure their proper function.

For some reason, the Main.qml file 'rewrites' itself resulting in one of two possible errors. You can clear this issue by editing the Main.qml file in the following way.. search for - Fader, and if found delete that whole section ...Fader...{ ..... } and battery, and if found, remove that section as well it is usually the word battery followed by empty curly braces.. ie: battery { }

I have made the Main.qml file READ ONLY so it can't rewrite itself and so far this has worked..

These are the only two possible causes for a theme to not render correctly. (that I'm aware of at this time)

INSTALL: Just copy the theme directory to '/usr/share/sddm/themes/' and you're ready to use them.

enjoy!