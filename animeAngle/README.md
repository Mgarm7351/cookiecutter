# Abstract Dark theme for SDDM

This theme was inspired by
[solarized-sddm-theme](https://github.com/MalditoBarbudo/solarized_sddm_theme)

#### Changing font or background in `theme.conf`:

example:
```
[General]
background=background.png
displayFont="Montserrat"
```

## Screenshot

![screenshot](animeAngel.png)

## License

Theme is licensed under GPL.
QML file is MIT licensed.
